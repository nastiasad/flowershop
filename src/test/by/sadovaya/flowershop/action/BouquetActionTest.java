package test.by.sadovaya.flowershop.action;

import by.sadovaya.flowershop.action.BouquetAction;
import by.sadovaya.flowershop.entity.Bouquet;
import by.sadovaya.flowershop.entity.CutFlower;
import by.sadovaya.flowershop.entity.WildFlower;
import by.sadovaya.flowershop.exceptions.WrongDataException;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.fail;

/**
 * Created by Анастасия on 27.09.2015.
 */
public class BouquetActionTest {
    private static final int expectedIndex = 0;
    private static final int stemLengthFirstElem = 60;
    private static final int firFlowerCost = 500;
    private static final int secFlowerCost = 1000;
    private static final int thirdFlowerCost = 200;

    @Test
    public void priceBouquetTest() throws WrongDataException {
        Bouquet bouquet = new Bouquet(createTestList());
        int actual = bouquet.price();
        int expected = firFlowerCost + secFlowerCost + thirdFlowerCost;
        Assert.assertEquals(actual, expected, 0.001);
    }

    @Test
    public void findFlowerInRangeTest() throws WrongDataException {
        BouquetAction bouquetAction = new BouquetAction();
        Bouquet bouquet = new Bouquet(createTestList());
        CutFlower actual = bouquetAction.findFlowerInRange(bouquet, stemLengthFirstElem - 5,
                stemLengthFirstElem + 5);
        CutFlower expected = bouquet.getBouquet().get(expectedIndex);
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void findFlowerInRangeTest2() throws WrongDataException{
        BouquetAction bouquetAction = new BouquetAction();
        Bouquet bouquet = new Bouquet(createTestList());
        CutFlower actual = bouquetAction.findFlowerInRange(bouquet, stemLengthFirstElem + 100,
                stemLengthFirstElem + 200);
        CutFlower expected = null;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void check(){
        int stemLength = -10;
        try {
            WildFlower flower = new WildFlower();
            flower.setStemLength(stemLength);
            fail("Test for stem length = " + stemLength + " should have thrown a WrongDataException");
        }catch (WrongDataException e) {
            Assert.assertEquals("The length of stem is not valid!", e.getMessage());
        }

    }

    private ArrayList<CutFlower> createTestList() throws WrongDataException{
        ArrayList<CutFlower> arrayForBouquet = new ArrayList<CutFlower>();
        arrayForBouquet.add(new WildFlower(firFlowerCost, "Belarus", stemLengthFirstElem,
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 20), "white", WildFlower.Type.CAMOMILE));
        arrayForBouquet.add(new WildFlower(secFlowerCost, "Belarus", 20,
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 16), "blue", WildFlower.Type.CORNFLOWER));
        arrayForBouquet.add(new WildFlower(thirdFlowerCost, "Russia", 78,
                new GregorianCalendar(2015, Calendar.SEPTEMBER, 22), "yellow", WildFlower.Type.SUNFLOWER));
        return arrayForBouquet;
    }
}
