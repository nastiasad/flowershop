package by.sadovaya.flowershop.entity;

import by.sadovaya.flowershop.exceptions.WrongDataException;

import java.util.GregorianCalendar;

/**
 * Created by Анастасия on 23.09.2015.
 */
public class GardenFlower extends CutFlower {
    public static enum Type {ROSE, TULIP, LILY, ORCHID, ASTRA};

    private Type type;
    private static final int PARENT_NAME_LENGTH = 12;

    public GardenFlower() {
    }

    public GardenFlower(int cost, String growingCountry, int stemLength, GregorianCalendar dateCut,
                        String color, Type type) throws WrongDataException {
        super(cost, growingCountry, stemLength, dateCut, color);
        this.type = type;
    }

    @Override
    public String toString() {
        return "Garden flower: " + super.toString().substring(PARENT_NAME_LENGTH)+
                ", type=" + type;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}
