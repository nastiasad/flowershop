package by.sadovaya.flowershop.entity;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Анастасия on 22.09.2015.
 */

public class Bouquet{
    private ArrayList<CutFlower> bouquet;

    public Bouquet(){
        bouquet = new ArrayList<CutFlower>();
    }

    public Bouquet(ArrayList<CutFlower> array){
        bouquet = array;
    }

    public ArrayList<CutFlower> getBouquet() {
        return bouquet;
    }

    public void setBouquet(ArrayList<CutFlower> bouquet) {
        this.bouquet = bouquet;
    }

    public void addFlower(CutFlower flower){
        bouquet.add(flower);
    }

    public int price(){
        int sum = 0;
        for (Iterator<CutFlower> i = bouquet.iterator(); i.hasNext(); ){
            sum += i.next().getCost();
        }
        return sum;
    }

}
