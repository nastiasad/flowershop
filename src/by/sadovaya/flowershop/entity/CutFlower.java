package by.sadovaya.flowershop.entity;

import by.sadovaya.flowershop.exceptions.WrongDataException;
import java.sql.DataTruncation;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;

import static java.util.Calendar.YEAR;

/**
 * Created by Анастасия on 22.09.2015.
 */
public class CutFlower extends Flower {

    private int stemLength;
    private GregorianCalendar dateCut;
    private String color;
    private static final int PARENT_NAME_LENGTH = 7;
    private static final String WRONG_LEN_MESSAGE = "The length of stem is not valid!";

    public CutFlower() {
    }

    public CutFlower(int cost, String growingCountry, int stemLength,
                     GregorianCalendar dateCut, String color) throws WrongDataException{
        super(cost, growingCountry);
        if (stemLength <= 0) {
            throw new WrongDataException(WRONG_LEN_MESSAGE);
        }
        this.stemLength = stemLength;
        this.color = color;
        this.dateCut = dateCut;
    }

    @Override
    public String toString() {
        return "Cut flower: " + super.toString().substring(PARENT_NAME_LENGTH) +
                ", color=" + color +
                ", stem length=" + stemLength + ", date of cut=" + dateCut.get(Calendar.DAY_OF_MONTH) +
                "." + (dateCut.get(Calendar.MONTH)+1) + "." + dateCut.get(YEAR);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public GregorianCalendar getDateCut() {
        return dateCut;
    }

    public void setDateCut(GregorianCalendar dateCut) {
        this.dateCut = dateCut;
    }

    public int getStemLength() {
        return stemLength;
    }

    public void setStemLength(int stemLength) throws WrongDataException {
        if(stemLength <= 0) {
            throw new WrongDataException(WRONG_LEN_MESSAGE);
        }
        this.stemLength = stemLength;
    }

    public static class CutFlowerComparator implements Comparator<CutFlower> {
        @Override
        public int compare(CutFlower o1, CutFlower o2) {
            return o1.getDateCut().compareTo(o2.getDateCut());
        }
    }

}
