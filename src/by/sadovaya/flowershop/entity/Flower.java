package by.sadovaya.flowershop.entity;

import by.sadovaya.flowershop.exceptions.WrongDataException;

/**
 * Created by Анастасия on 22.09.2015.
 */
public class Flower {
    private int cost;
    private String growingCountry;
    private static final String WRONG_COST_MESSAGE = "The cost is not valid!";

    public Flower (){
    }

    public Flower(int cost, String growingCountry) throws WrongDataException {
        if (cost < 0) {
            throw new WrongDataException(WRONG_COST_MESSAGE);
        }
        this.cost = cost;
        this.growingCountry = growingCountry;
    }

    @Override
    public String toString() {
        return "Flower: " +
                "cost=" + cost +
                ", growingCountry=" + growingCountry;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) throws WrongDataException {
        if(cost < 0) {
            throw new WrongDataException(WRONG_COST_MESSAGE);
        }
        this.cost = cost;
    }

    public String getGrowingCountry() {
        return growingCountry;
    }

    public void setGrowingCountry(String growingCountry) {
        this.growingCountry = growingCountry;
    }
}
