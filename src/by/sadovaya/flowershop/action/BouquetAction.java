package by.sadovaya.flowershop.action;

import by.sadovaya.flowershop.entity.Bouquet;
import by.sadovaya.flowershop.entity.CutFlower;
import org.apache.log4j.Logger;

import java.util.Collections;

/**
 * Created by Анастасия on 23.09.2015.
 */
public class BouquetAction {
    private static Logger logger = Logger.getLogger(BouquetAction.class);

    public void sortBouquet (Bouquet bouquet){
        Collections.sort(bouquet.getBouquet(), new CutFlower.CutFlowerComparator());
    }

    public CutFlower findFlowerInRange(Bouquet bouquet, int minLength, int maxLength) {
        CutFlower result = null;
        for (CutFlower flower : bouquet.getBouquet()) {
            if (flower.getStemLength() >= minLength && flower.getStemLength() <= maxLength) {
                result = flower;
            }
        }
        if (result == null) {
            logger.info("Flower in range [" + minLength + ", " + maxLength + "] not found.");
        }
        return result;
    }
}
