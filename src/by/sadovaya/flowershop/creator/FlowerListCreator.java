package by.sadovaya.flowershop.creator;

import by.sadovaya.flowershop.entity.CutFlower;
import by.sadovaya.flowershop.entity.GardenFlower;
import by.sadovaya.flowershop.entity.WildFlower;
import by.sadovaya.flowershop.exceptions.WrongDataException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Анастасия on 27.09.2015.
 */
public class FlowerListCreator {
    public ArrayList<CutFlower> createArray() throws WrongDataException{
        ArrayList<CutFlower> arrayForBouquet = new ArrayList<CutFlower>();

        arrayForBouquet.add(new WildFlower(500, "Belarus", 60, new GregorianCalendar(2015, Calendar.SEPTEMBER, 20),
                "white", WildFlower.Type.CAMOMILE));
        arrayForBouquet.add(new WildFlower(1000, "Belarus", 20, new GregorianCalendar(2015, Calendar.SEPTEMBER, 16),
                "blue", WildFlower.Type.CORNFLOWER));
        arrayForBouquet.add(new WildFlower(200, "Russia", 78, new GregorianCalendar(2015, Calendar.SEPTEMBER, 22),
                "yellow", WildFlower.Type.SUNFLOWER));
        arrayForBouquet.add(new WildFlower(520, "Belarus", 53, new GregorianCalendar(2015, Calendar.SEPTEMBER, 24),
                "white", WildFlower.Type.CAMOMILE));
        arrayForBouquet.add(new WildFlower(410, "Ukraine", 82, new GregorianCalendar(2015, Calendar.SEPTEMBER, 19),
                "yellow", WildFlower.Type.SUNFLOWER));
        arrayForBouquet.add(new GardenFlower(1000, "Holland", 74, new GregorianCalendar(2015, Calendar.SEPTEMBER, 20),
                "red", GardenFlower.Type.ROSE));
        arrayForBouquet.add(new GardenFlower(1500, "Japan", 53, new GregorianCalendar(2015, Calendar.SEPTEMBER, 15),
                "white and yellow", GardenFlower.Type.ORCHID));
        arrayForBouquet.add(new GardenFlower(590, "Poland", 35, new GregorianCalendar(2015, Calendar.SEPTEMBER, 22),
                "purple", GardenFlower.Type.TULIP));
        arrayForBouquet.add(new GardenFlower(1500, "Holland", 67, new GregorianCalendar(2015, Calendar.SEPTEMBER, 22),
                "blue", GardenFlower.Type.ROSE));
        arrayForBouquet.add(new GardenFlower(700, "Belarus", 81, new GregorianCalendar(2015, Calendar.SEPTEMBER, 23),
                "orange", GardenFlower.Type.LILY));
        return arrayForBouquet;
    }
}
