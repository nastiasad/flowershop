package by.sadovaya.flowershop.runner;

import by.sadovaya.flowershop.action.BouquetAction;
import by.sadovaya.flowershop.creator.FlowerListCreator;
import by.sadovaya.flowershop.exceptions.WrongDataException;
import by.sadovaya.flowershop.entity.Bouquet;
import by.sadovaya.flowershop.entity.CutFlower;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by Анастасия on 23.09.2015.
 */
public class Runner {
    static {
        new DOMConfigurator().doConfigure("log4j.xml", LogManager.getLoggerRepository());
    }
    private static Logger logger = Logger.getLogger(Runner.class);
    private static final int MIN_STEM_RANGE = 100;
    private static final int MAX_STEM_RANGE = 200;


    public static void main(String[] args) {
        Bouquet bouquet = null;
        try {
            bouquet = new Bouquet(new FlowerListCreator().createArray());
            logger.info("Bouquet price: " + bouquet.price());
            new BouquetAction().sortBouquet(bouquet);
            logger.info("Sorted bouquet: ");
            for(CutFlower i: bouquet.getBouquet()){
                logger.info(i.toString());
            }
            CutFlower flowerInRange = new BouquetAction().findFlowerInRange(bouquet, MIN_STEM_RANGE, MAX_STEM_RANGE);
            if (flowerInRange != null){
                logger.info("Flower in range [" + MIN_STEM_RANGE + ", " + MAX_STEM_RANGE + "]: \n" +
                        flowerInRange.toString());
            }
        } catch (WrongDataException e) {
            logger.error("Error found!", e);
        }
        logger.info("\n\n");
    }
}
